﻿using UnityEngine;

namespace com.studio.app
{
    public class CharacterUnit : BaseUnit
    {
        public static CharacterUnit selectedUnit;

        private Vector3 unitDirection = Vector3.zero;
        private Vector3 unitTarget = Vector3.positiveInfinity;

        public float speed = 0.05f;

        public override void selectUnit()
        {
         
            Renderer[] renderer = this.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < renderer.Length; i++)
            {
                renderer[i].sharedMaterial = Instantiate(Resources.Load<Material>("outline"));
                renderer[i].sharedMaterial.color = this._color;
            }
        }
        public override void unselectUnit()
        {
            
            Renderer[] renderer = this.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < renderer.Length; i++)
            {
                renderer[i].sharedMaterial = Instantiate(Resources.Load<Material>("blink"));;
                renderer[i].sharedMaterial.color = this._color;
            }
            
        }

        public override Color getColor()
        {
            return this._color;
        }

        public override void setBlinkColor(Color color)
        {
            //this.gameObject.GetComponent<Renderer>().material.SetColor("_BlinkColor", color);
            Renderer[] renderer = this.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < renderer.Length; i++)
            {
                renderer[i].sharedMaterial.SetColor("_BlinkColor", color);
            }
        }

        public override void walkUnit(Vector3 to)
        {
            this.GetComponent<Animator>().Play("walk");
            this.unitDirection = to - this.gameObject.transform.position;
            this.unitTarget = to;
            this.transform.rotation = Quaternion.LookRotation(unitDirection);
        }

        public override void setType(int type, Color color)
        {
            this._color = color;
            this.typeUnit = type;
            
            Renderer[] renderer = this.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < renderer.Length; i++)
            {
                renderer[i].sharedMaterial = Instantiate(Resources.Load<Material>("blink"));;
                //renderer[i].sharedMaterial.SetColor("_Color", this._color);//color = this._color;
                renderer[i].sharedMaterial.color = this._color;

            }
            
        }

        void Update()
        {
            
            
            if (!this.unitDirection.Equals(Vector3.zero))
            {
               
                if ((unitTarget - this.gameObject.transform.position).magnitude <
                    (this.unitDirection * speed * Time.deltaTime).magnitude)
                {
                    this.gameObject.transform.position = this.unitTarget;
                    this.unitTarget = Vector3.positiveInfinity;
                    this.unitDirection = Vector3.zero;
                    this.GetComponent<Animator>().Play("stop");

                    GameEvent.UnitEvent.Invoke(UserAction.EndMove, this);
                }
                else
                {
                    this.unitDirection.Normalize();

                    this.gameObject.transform.position += this.unitDirection * this.speed * Time.deltaTime;
                }
            }
        }
    }
}