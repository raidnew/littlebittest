using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace com.studio.app
{
    public class CubeUnit : BaseUnit
    {

        private bool isMoved = false;
        private Vector3 prevPos = Vector3.negativeInfinity;
        private List<float> deltaPos = new List<float>();
        private int currentIndex;

        public CubeUnit():base()
        {
            
        }
        
        public override Color getColor()
        {
            return this._color;
        }
        
        public override void beginMove(Vector3 start, Vector3 end, float time)
        {
            Vector3 force = end - start;
            this.gameObject.GetComponent<Rigidbody>().AddForce((force) / time * 10);
            this.isMoved = true;
        }

        public override void setType(int type, Color color)
        {
            Renderer render = this.GetComponent<Renderer>();
            //Material test = Resources.Load<Material>("blink");
            //render.sharedMaterial = new Material(Shader.Find("LittleBit/BlinkShader"));
            render.sharedMaterial = Resources.Load<Material>("blink");
            //render.sharedMaterial.color = color;
            this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", color);
            this._color = color;
            this.typeUnit = type;
        }

        public override void setBlinkColor(Color color)
        {
            this.gameObject.GetComponent<Renderer>().material.SetColor("_BlinkColor", color);
        }
        
        void Update()
        {

            if (isMoved)
            {
                this.deltaPos.Add( Vector3.Distance(prevPos, this.transform.position));
                prevPos = this.transform.position;
                if (deltaPos.Count > 10) deltaPos.RemoveAt(0);

                prevPos = this.gameObject.transform.position;

                float sum = this.deltaPos.Sum();
                if (sum == 0)
                {
                    GameEvent.UnitEvent.Invoke(UserAction.EndMove, this);
                    isMoved = false;
                }
            }
            
            
        }
    }
}
