﻿using UnityEngine;

namespace com.studio.app
{
    public class BaseArea
    {
        public int type { get; protected set; }

        public virtual void drawArea(Texture2D levelArea)
        {

        }

        public virtual bool unitOnArea(Vector3 unitPosition)
        {
            return false;
        }
    }
}
