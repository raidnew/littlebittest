using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace com.studio.app
{
    public class LevelController
    {
        // Start is called before the first frame update
        private List<BaseArea> areas;
        private List<BaseUnit> unitsOnLevel;
        private List<ParticleSystem> particlesOnLevel;
        
        private Texture2D fieldImage;
        private Bounds planeBounds;
        public GameObject gamePlane;
        
        public Color[] colors;

        private string unitType;

        public Material materialBlink;
        public Material materialOutline;
        
        public LevelController()
        {
            GameEvent.UnitEvent.AddListener(unitHandler);
        }

        private void unitHandler(UserAction action, BaseUnit unit)
        {
            if (action == UserAction.EndMove)
            {
                this.checkUnitProgress();
            }
        }

        public float checkUnitProgress()
        {
            float unitOnSelfArea = 0;
            Vector3 unitPosition;

            for (int iUnit = 0; iUnit < unitsOnLevel.Count; iUnit++)
            {
                unitsOnLevel[iUnit].setBlinkColor(Color.red);
                unitPosition = unitsOnLevel[iUnit].gameObject.transform.position;
                for (int iRect = 0; iRect < areas.Count; iRect++)
                {
                    if (areas[iRect].type == unitsOnLevel[iUnit].typeUnit &&
                        areas[iRect].unitOnArea(unitPosition))
                    {
                        unitOnSelfArea++;
                        unitsOnLevel[iUnit].setBlinkColor(Color.white);
                    }
                }
            }
            float progress = unitOnSelfArea / unitsOnLevel.Count;
            return progress;
        }

        public void generateLevel()
        {
            colors = new[] {Color.red, Color.green, Color.blue};
            this.areas = new List<BaseArea>();
            planeBounds = gamePlane.GetComponent<MeshRenderer>().bounds;
            int x = 0, y = 0, width = 50, height = 50;
            Vector3 boundsMin = this.gamePlane.GetComponent<MeshRenderer>().bounds.min;
            for (int i = 0; i < colors.Length; i++)
            {
                if ((x + 1) * width > planeBounds.max.x - planeBounds.min.x)
                {
                    x = 0;
                    y++;
                }
                areas.Add(new RectArea(i, colors[i], new Rect(x * width, y * height, width, height), boundsMin));
                x++;
            }
            Material[] test = gamePlane.GetComponent<Renderer>().materials;
        }

        private void generateUnits(int unitCount)
        {
            this.unitsOnLevel = new List<BaseUnit>();

            for (int i = 0; i < unitCount; i++)
            {
                int color = Random.Range(0, 2);
                fabricUnit(this.unitType, color);
            }

            checkUnitProgress();
        }

        private void fabricUnit(String type, int color)
        {
            GameObject unitPrefab = null;
            switch (type)
            {
                case "Cube":
                    unitPrefab = Resources.Load<GameObject>("prefabs/Units/Cube");
                    break;
                case "Character":
                    unitPrefab = Resources.Load<GameObject>("prefabs/Units/Characters");
                    break;
            }

            GameObject unitView = GameObject.Instantiate(unitPrefab);
            Bounds unitMinBound = unitView.GetComponentInChildren<Renderer>().bounds;
            unitView.GetComponent<BaseUnit>()
                .moveUnit(new Vector3(Random.Range(5, 95) - 50, -unitMinBound.min.y, Random.Range(5, 95) - 50));
            unitsOnLevel.Add(unitView.GetComponent<BaseUnit>());
            int typeUnit = Random.Range(0, colors.Length);
            unitView.GetComponent<BaseUnit>().setType(typeUnit, colors[typeUnit]);

        }

        private ParticleSystem createParticle(Vector3 posiiton, Color clr)
        {
            GameObject particleContainer = new GameObject("ParticleContainer");
            particleContainer.transform.position = new Vector3(posiiton.x, posiiton.y, posiiton.z);
            ParticleSystem particle = particleContainer.AddComponent<ParticleSystem>();
            particle.Stop();

            Material newMat = new Material(Shader.Find("Standard"));
            newMat.color = clr;
            
            particle.gameObject.GetComponent<ParticleSystemRenderer>().material = newMat;
            
            ParticleSystem.MainModule particleMain = particle.main;
            particleMain.duration = 0.2f;
            particleMain.loop = false;
            particleMain.startSpeed = 20f;
            particleMain.startLifetime = 1f;
            particleMain.startSize = 0.4f;

            ParticleSystem.EmissionModule particleEmission = particle.emission;
            particleEmission.rateOverTime = 200;

            ParticleSystem.ShapeModule shape = particle.shape;
            shape.angle = 13f;
            shape.rotation = new Vector3(-90, 0, 0);
            particle.Play();

            return particle;
        }
        
        public void destroyLevel()
        {
            particlesOnLevel = new List<ParticleSystem>();
            
            for (int i = 0; i < this.unitsOnLevel.Count; i++)
            {
                particlesOnLevel.Add(createParticle(this.unitsOnLevel[i].gameObject.transform.position, this.unitsOnLevel[i].getColor()));
                GameObject.Destroy(this.unitsOnLevel[i].gameObject);
            }
        }

        public void clearLevel()
        {
            for (int i = 0; i < this.particlesOnLevel.Count; i++)
            {
                GameObject.Destroy(this.particlesOnLevel[i].gameObject);
            }
        }
        
        public void initLevel(int currentlevelIndex, string unitType, int countUnits)
        {
            this.unitType = unitType;
            generateLevel();
            this.fieldImage = new Texture2D((int) (planeBounds.max.x - planeBounds.min.x),
                (int) (planeBounds.max.z - planeBounds.min.z));
            this.gamePlane.GetComponent<Renderer>().material.mainTexture = this.fieldImage;
            this.gamePlane.GetComponent<Renderer>().material.mainTextureScale = new Vector2(-1, -1);
            drawField();
            generateUnits(countUnits);
        }

        private void drawField()
        {
            for (int i = 0; i < areas.Count; i++)
            {
                areas[i].drawArea(this.fieldImage);
            }
        }

    }
}