﻿using UnityEngine;

namespace com.studio.app
{
    public class Type
    {
        private static int _id = 0;

        public int id { get; }
        public Color color { get; }

        Type(Color color)
        {
            this.id = Type._id++;
            this.color = color;
        }
    }
}
