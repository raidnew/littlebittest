﻿using UnityEngine.Events;

namespace com.studio.app
{
    public enum UserAction
    {
        EndMove,
        SetArea,
        Die
    }

    public class UnitEvent : UnityEvent<UserAction, BaseUnit>
    {
    }

    public class GameEvent
    {
        private static GameEvent __instance;
        private UnitEvent unitEvent;

        public static UnitEvent UnitEvent
        {
            get
            {
                if (__instance == null)
                {
                    __instance = new GameEvent();
                }
                return __instance.unitEvent;
            }
        }

        public static void Invoke()
        {

        }

        public GameEvent()
        {
            __instance = this;
            this.unitEvent = new UnitEvent();
        }
    }
}
