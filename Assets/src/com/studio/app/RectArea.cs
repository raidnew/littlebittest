﻿using UnityEngine;

namespace com.studio.app
{
    public class RectArea : BaseArea
    {

        private Rect area;
        public Rect gameArea;
        protected Color color;

        public RectArea(int type, Color color, Rect rect, Vector3 offsetArea)
        {
            this.type = type;
            this.color = color;
            this.gameArea = new Rect(rect.x + offsetArea.x, rect.y + offsetArea.z, rect.width, rect.height);
            this.area = new Rect(rect.x, rect.y, rect.width, rect.height);

        }

        public override bool unitOnArea(Vector3 unitPosition)
        {
            if ((unitPosition.x > this.gameArea.x && unitPosition.x < this.gameArea.x + this.gameArea.width) && (
                unitPosition.z > this.gameArea.y && unitPosition.z < this.gameArea.y + this.gameArea.height))
                return true;

            return false;
        }

        public override void drawArea(Texture2D levelArea)
        {
            Color32[] pixels = new Color32[(int) (area.width * area.height)];
            for (int i = 0; i < pixels.Length; i++)
            {
                pixels[i] = color;
            }

            levelArea.SetPixels32((int) area.x, (int) area.y, (int) area.width, (int) area.height, pixels);
            levelArea.Apply();
        }
    }
}