using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace com.studio.app
{
    public class BaseUnit : MonoBehaviour
    {
        private bool wasMove;

        public int unitId;// { get; private set; }
        protected Color _color;

        private static int __id = 0;
        
        public int typeUnit { get; protected set; }

        public Material material;

        public BaseUnit()
        {
            this.unitId = BaseUnit.__id++;
        }
        public virtual void beginMove(Vector3 start, Vector3 end, float time)
        {

        }

        public virtual Color getColor()
        {
            return Color.clear;
        }
        
        public virtual void setBlinkColor(Color color)
        {

        }        
        
        public virtual void selectUnit()
        {

        }        
        public virtual void unselectUnit()
        {

        }

        public virtual void walkUnit(Vector3 to)
        {

        }

        public virtual void setType(int type, Color color)
        {
        }

        public void moveUnit(Vector3 newPosition, float speed = -1)
        {
            if (speed < 0)
            {
                this.gameObject.transform.position = new Vector3(newPosition.x, newPosition.y, newPosition.z);
            }
        }

        public void unitEndWalk()
        {

        }

        public bool checkOnArea()
        {
            return false;
        }
    }
}