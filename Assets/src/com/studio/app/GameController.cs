﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace com.studio.app
{
    public class GameController : MonoBehaviour
    {
        private LevelController _levelController;
        public GameObject gamePlane;

        private Plane planeForCalc;
        private BaseUnit selectedUnit;

        public GameEvent gameEvent;

        public Image progressBar;
        public Text currentLevel;
        public Text nextLevel;

        private float _xSpeed, _ySpeed;

        public Material blinkMaterial;
        public Material outlineMaterial;

        private int currentLevelIndex = 0;

        private float minYCam;
        private float maxYCam;
        private float camVectorLength;
        
        void Awake()
        {
            _levelController = new LevelController();
            _levelController.gamePlane = gamePlane;
            _levelController.materialBlink = blinkMaterial;
            _levelController.materialOutline = outlineMaterial;
            planeForCalc = new Plane(gamePlane.transform.up, gamePlane.transform.position);
            MouseController.getInstance().SwipeAction.AddListener(SwipeHandler);
            MouseController.getInstance().MouseAction.AddListener(MouseHandler);
            GameEvent.UnitEvent.AddListener(unitHandler);
            initLevel(1);
        }

        private void endLevel()
        {
            _levelController.destroyLevel();
        }
        private void initLevel(int level)
        {
            bool endGame = false;
            this.currentLevelIndex = level;
            currentLevel.text = currentLevelIndex.ToString();
            nextLevel.text = (currentLevelIndex+1).ToString();
            switch (level)
            {
                case 1:
                    _levelController.initLevel(this.currentLevelIndex, "Character", 10);
                    break;
                case 2:
                    _levelController.initLevel(this.currentLevelIndex, "Cube", 10);
                    break;
                default:
                    endGame = true;
                    break;
            }

            if (!endGame)
            {
                this.checkProgress();
            }
        }

        private void checkProgress()
        {
            float progress = _levelController.checkUnitProgress();
            this.progressBar.fillAmount = progress;

            if (progress == 1)
            {
                this.levelComplele();
            }
        }
        
        private void unitHandler(UserAction e, BaseUnit unit)
        {
            if (e == UserAction.EndMove)
            {
                this.checkProgress();
            }
        }

        private void levelComplele()
        {
            endLevel();
            StartCoroutine(delayStartlevel(2f));
            
        }
        
        private IEnumerator delayStartlevel(float lifetime)
        {
            yield return new WaitForSeconds(lifetime);
            _levelController.clearLevel();
            initLevel(this.currentLevelIndex + 1);
        }
        
        private void MouseHandler(Vector2 point, MouseActions action)
        {
            if (action == MouseActions.Click) raycastClick(point);
        }

        private void SwipeHandler(Vector2 start, Vector2 end, float time)
        {
            raycastSwipe(start, end, time);
        }

        private Vector3 convertScreenToPlaneCoordinates(Vector2 screenPoint)
        {
            RaycastHit[] hits;
            Ray ray = Camera.main.ScreenPointToRay(screenPoint);

            //TODO calc distance form cam to far far away point plane
            hits = Physics.RaycastAll(ray, 200f);
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].collider.name == gamePlane.name)
                {
                    return hits[i].point;
                }
            }

            return Vector3.negativeInfinity;
        }

        private void raycastClick(Vector2 point)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(point);
            Physics.Raycast(ray, out hit);
            //Click to unit

            if (selectedUnit)
            {
                if (selectedUnit.gameObject.GetComponent<CharacterUnit>())
                {
                    selectedUnit.gameObject.GetComponent<CharacterUnit>().unselectUnit();
                }
            }
            
            if (hit.collider != null && hit.collider.gameObject.GetComponent<CharacterUnit>())
            {
                hit.collider.gameObject.GetComponent<CharacterUnit>().selectUnit();
                selectedUnit = hit.collider.gameObject.GetComponent<BaseUnit>();
            }
            else if (selectedUnit != null)
            {
                if (selectedUnit.gameObject.GetComponent<CharacterUnit>())
                {
                    selectedUnit.gameObject.GetComponent<CharacterUnit>().unselectUnit();
                }

                float distance = planeForCalc.GetDistanceToPoint(selectedUnit.gameObject.transform.position);
                Vector3 targetPoint = convertScreenToPlaneCoordinates(point);
                targetPoint = targetPoint + planeForCalc.normal * distance;
                selectedUnit.walkUnit(targetPoint);
                selectedUnit = null;
            }
        }

        private void raycastSwipe(Vector2 start, Vector2 end, float time)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(start);
            Physics.Raycast(ray, out hit);
            if (hit.collider != null && hit.collider.gameObject.GetComponent<CubeUnit>()) //Swipe cube
            {
                Vector3 startPlane = convertScreenToPlaneCoordinates(start);
                Vector3 endPlane = convertScreenToPlaneCoordinates(end);

                if (!startPlane.Equals(Vector3.negativeInfinity) && !endPlane.Equals(Vector3.negativeInfinity))
                {
                    hit.collider.gameObject.GetComponent<CubeUnit>().beginMove(convertScreenToPlaneCoordinates(start),
                        convertScreenToPlaneCoordinates(end), time);
                }
            }
            else
            {
                _xSpeed = (end.x - start.x) / 10;
                _ySpeed = -(end.y - start.y) / 10;
            }
        }

        void Start()
        {
            minYCam = 10;
            maxYCam = Camera.main.transform.position.magnitude * Mathf.Cos((float) (Math.PI / 180 * 70));
            camVectorLength = Camera.main.transform.position.magnitude;
        }

        void Update()
        {
            if (_xSpeed != 0 || _ySpeed != 0)
            {
                if (Camera.main.transform.position.y < minYCam && _ySpeed < 0)
                {
                    _ySpeed = 0;
                }else if (Camera.main.transform.position.y > maxYCam && _ySpeed > 0)
                {
                    _ySpeed = 0;
                }

                Camera.main.transform.RotateAround(Vector3.zero, Camera.main.transform.right, _ySpeed);
                Camera.main.transform.RotateAround(Vector3.zero, transform.up, _xSpeed);

                _ySpeed *= 0.95f;
                _xSpeed *= 0.95f;

                if (Mathf.Abs(_xSpeed) < 0.2f) _xSpeed = 0;
                if (Mathf.Abs(_ySpeed) < 0.2f) _ySpeed = 0;
            }

        }
    }
}