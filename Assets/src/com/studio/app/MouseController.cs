using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SwipeEvent : UnityEvent<Vector2, Vector2, float>
{
}

public class MouseEvent : UnityEvent<Vector2, MouseActions>
{
}

public enum MouseActions {
    Click,
    DoubleClick
}

public class MouseController : MonoBehaviour
{
    private static MouseController __instance;
    
    public SwipeEvent SwipeAction;
    public MouseEvent MouseAction;
    
    private Vector3 startMove;
    private float timeStartTouch;

    public static MouseController getInstance()
    {
        return MouseController.__instance;
    }

    private MouseController()
    {
        if(MouseController.__instance != null) throw new Exception("MouseController is Singelton");
        MouseController.__instance = this;
        this.SwipeAction = new SwipeEvent();
        this.MouseAction = new MouseEvent();
    }
    
    private void startTouch(Vector3 mousePos)
    {
        startMove = mousePos;
        timeStartTouch = Time.time;
    }

    private void endTouch(Vector3 mousePos)
    {
        if ((mousePos - startMove).magnitude > 20 && Time.time - timeStartTouch < 0.5f)
        {
            SwipeAction.Invoke(startMove, mousePos, Time.time - timeStartTouch);
        }
        else
        {
            MouseAction.Invoke(mousePos, MouseActions.Click);
        }
    }

    void Awake()
    {
        
    }
    
    void Start()
    {
        //SwipeAction = new SwipeEvent();
    }

    void Update()
    {

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            
            if (touch.phase == TouchPhase.Began)
            {
                startTouch(touch.position);
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                endTouch(touch.position);
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            startTouch(Input.mousePosition);
        }
        if (Input.GetMouseButtonUp(0))
        {
            endTouch(Input.mousePosition);
        }
    }
}
